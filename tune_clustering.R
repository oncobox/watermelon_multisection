random_trials_grid <- 500
bootstrap_trials_grid <- 500

# paths to data
file_gallowplot <- "./utils/gallow_plot.R"
file_hlth_tissues <- "./data/healthy_tissues.txt"
file_hlth_tissue_class <- "./data/healthy_tissue_sources.txt"

# a function which draws a bar plot underneath a dendrogram
source(file_gallowplot)

# preparing healthy tissues' data
hlth_tissues_raw <- read_tsv(file_hlth_tissues)
genes <- hlth_tissues_raw[["gene_names"]]
hlth_tissues_raw %<>%
    select(-1) %>% data.matrix() %>%
    set_rownames(genes)
colnames(hlth_tissues_raw) %<>% str_to_upper()
# sequencing depth of each sample
hlth_tissues_depths <- colSums(hlth_tissues_raw)
# R names cannot begin with number so it places X in the beginning
norm_names_clean <- str_remove_all(colnames(hlth_tissues_raw), "^X")

# tissue sources
hlth_tissues_anno <-
    read_tsv(file_hlth_tissue_class) %>%
    mutate(`RNAseq file name` = str_to_upper(`RNAseq file name`)) %>%
    filter(`RNAseq file name` %in% norm_names_clean)

classes_2examine <- setNames(hlth_tissues_anno[["Sample source"]], hlth_tissues_anno[["RNAseq file name"]])[norm_names_clean]
# grouping small classes into one (r = reduced)
classes_2examine_r <-
    fct_other(
        classes_2examine,
        drop = c("Uterus (myometrium)", "Cervix", "Ovary"),
        other_level = "Ovary and cervix"
    )

# DESeq scaling
hlth_tissues_sf <- DESeq2::estimateSizeFactorsForMatrix(hlth_tissues_raw)
hlth_tissues <-
    {t(hlth_tissues_raw) / hlth_tissues_sf} %>%
    t() %>% inset(. == 0, 1)

hc_norms <-
    hlth_tissues %>%
    log10() %>% t() %>% dist() %>%
    hclust(method = "ward.D2")

# select genes ranked by sum(counts)
# conveniently generates feature sets
percentile_fsets <- function(data_mat, rowfun, percentiles = seq(0, 1, by = 0.1)) {
    row_stats <- apply(data_mat, 2, rowfun)
    row_stats_q <-
        quantile(row_stats, percentiles) %>%
        setNames(str_c("q", percentiles*100))

    fsets <- list()
    for (qq in names(row_stats_q)) {
        fsets[[qq]] <- sort(rownames(data_mat)[row_stats >= row_stats_q[qq]])
    }
    return(fsets)
}

perc <- seq(0, 1, by = 0.2)
fsets_cnt <- percentile_fsets(hlth_tissues_raw, sum, perc)

if (!file.exists(str_c(path_out, "wm_grid_cnt.txt"))) {
    wm_grid_cnt <-
        watermelon_on_grid(
            data_mat = log10(hlth_tissues),
            classes = classes_2examine_r,
            feature_sets = fsets_cnt,
            random_trials = random_trials_grid,
            bootstrap_trials = bootstrap_trials_grid
        )
    write_tsv(wm_grid_cnt, str_c(path_out, "wm_grid_cnt.txt"))
} else {
    wm_grid_cnt <- read_tsv(str_c(path_out, "wm_grid_cnt.txt"))
}
# ordering quantiles
wm_grid_cnt[["feature_set"]] %<>% parse_factor(str_c("q", seq(0, 100, 10)))

# top-ranking algorithms / feature sets
plot_grid_result(wm_grid_cnt, 10, size = 0.75) +
    ggsave(str_c(path_plot, "grid_cnt.png"))

# heatmap with a full overview of the result
heatmap_grid_result(wm_grid_cnt) +
    theme(
        axis.title.x = element_text(size = 24),
        axis.title.y = element_text(size = 24),
        legend.title = element_text(size = 16),
        legend.text = element_text(size = 10),
        axis.text.x = element_text(size = 16),
        axis.text.y = element_text(size = 16)
    ) +
    ggsave(str_c(path_plot, "heat_cnt.png"), height = 7, width = 10)

# N counts filtering: best and average cases
hc_best_cnt <-
    hlth_tissues %>%
    magrittr::extract(fsets_cnt[["q20"]], ) %>%
    log10() %>% t() %>% dist() %>%
    hclust(method = "ward.D")
# more informative labels
hc_best_cnt[["labels"]] %<>% str_c(classes_2examine_r, "__", .)

gallow_plot(
    hc_best_cnt,
    log10(hlth_tissues_depths),
    # class names are too long to fit onto a single row
    str_replace_all(classes_2examine_r, " ", "\n"),
    y_bar_breaks = 0:7,
    legend_nrow = 2,
    font_size_labs = 5,
    label_y_hist = "log10 sequencing depth",
    font_size_y_text = 40,
    font_size_y_name = 40,
    legend_key_size = 1.3,
    font_size_legend = 20,
    resolution = c(2560, 1440),
    plot_name = str_c(path_plot, "gallow_best_cnt.png")
)

# average case
hc_avg_cnt <-
    hlth_tissues %>%
    magrittr::extract(fsets_cnt[["q100"]], ) %>%
    log10() %>% t() %>% dist() %>%
    hclust(method = "complete")
# more informative labels
hc_avg_cnt[["labels"]] %<>% str_c(classes_2examine_r, "__", .)

gallow_plot(
    hc_avg_cnt,
    log10(hlth_tissues_depths),
    str_replace_all(classes_2examine_r, " ", "\n"),
    # method = "complete",
    y_bar_breaks = 0:7,
    legend_nrow = 2,
    font_size_labs = 5,
    label_y_hist = "log10 sequencing depth",
    font_size_y_text = 40,
    font_size_y_name = 40,
    legend_key_size = 1.3,
    font_size_legend = 20,
    resolution = c(2560, 1440),
    plot_name = str_c(path_plot, "gallow_mean_cnt.png")
)